# LAB Gitlab AI
## Content
- Generate issue descriptions
- Issue summary
- Generate merge eequest template/code change summary
- Root Cause Analysis
- Vulnerability explanation/resolving
- Value stream forecasting
- Gitlab Duo Chat

## Use `Generate issue descriptions`
1. Create project in gitlab or Open your project
2. On the menu bar, click on 'Issues'
3. Click on 'New issue'
4. Click on 'Tanuki face'
5. Enter text of direction, main idea of the description 

    ``` yaml
    Title : Connecting to SQL Database
    
    Populate issue description : Problem to connect SQL database
                                 Can not connect to SQL Server database hosted on localhost from Kubernetes
    ```
6. Customize other settings and press 'Create issue'

## Use `Issue summary`
When you have any activity in issue like comment
1. Enter text comment 

     ``` yaml

    - I am trying to deploy an asp.net core 2.2 application in Kubernetes. This application is a simple web page that need an access to an SQL Server database to display some information. This database is hosted on my local development computer (localhost) and the web application is deployed in a minikube cluster to simulate the production environment where my web application could be deployed in a cloud and access a remote database.

    - I managed to display my web application by exposing port 80. However, I can't figure out how to make my web application connect to my SQL Server database hosted on my local computer from inside the cluster.

    - I assume that my connection string is correct since my web application can connect to the SQL Server database when I deploy it on an IIS local server, in a docker container (docker run) or a docker service (docker create service) but not when it is deployed in a Kubernetes cluster. I understand that the cluster is in a different network so I tried to create a service without selector as described in this question, but no luck... I even tried to change the connection string IP address to match the one of the created service but it failed too.

    - My firewall is setup to accept inbound connection to 1433 port.
      My SQL Server database is configured to allow remote access.
      Here is the connection string I use:"Server=172.24.144.1\MyServer,1433;Database=TestWebapp;User Id=user_name;Password=********;"

      ```  

2. Click on 'View summary' to generate summary of all comments

## Use `Generate merge eequest template/code change summary`
1. On the menu bar, click on 'Code' >> 'Brances'
2. Click on 'New Brance' 
3. Give 'Branch name = adding-stages' creat from 'basic-pipeline'
4. Edit with WebIDE
5. Go to `.gitlab-ci.yml`
6. Custom 

    ``` yaml
    stages:
      - build
      - test
      - release
      - internal
      - alpha
      - beta
      - deploy
    ```

    ``` yaml
    .promote_job:
      when: manual
      dependencies: []
      before_script:
        - echo "Need to add ..."  
   
    publishInternal:
      extends: .promote_job
      stage: internal
      variables:
        ENV: internal-env
      script:
        - echo "Deploying to internal environment $ENV" 

    promoteAlpha:
      extends: .promote_job
      stage: alpha
      variables:
        ENV: alpha-env
      script:
        - echo "Deploying to alpha environment $ENV" 

    promoteBeta:
      extends: .promote_job
      stage: beta
      variables:
        ENV: beta-env
      script:
        - echo "Deploying to beta environment $ENV" 

    deploy-staging:
      extends: .promote_job
      stage: deploy
      variables:
        ENV: staging
      environment: staging
      script:
        - echo "Deploying to environment $ENV"

    deploy-prod:
      extends: .promote_job
      stage: deploy
      variables:
        ENV: prod
      environment: production
      script:
        - echo "Deploying to environment $ENV"
    ```
6. Commit  
7. On the menu bar, click on 'Merge request' 
8. Click on 'New merge request' 
9. Source branch select 'adding-stages',  Target branch select 'basic-pipeline'
10. Click on 'Compare branches and continue'
11. Click on 'Tanuki face' to get merge request template or Click on 'Summarize code changes' 

## Use `Root Cause Analysis`
1. On the menu bar, click on 'Build' >> 'Pipelines'
2. Go to 'Failed' or 'Warning' pipeline
3. Click on 'stage' that 'Failed' or 'Warning'
4. Click on 'Root cause analysis' to analyzes why a job may have failed 

## Use `Vulnerability explanation/resolving`
1. On the menu bar, click on 'Secure' >> 'Vulnerability report'
2. Select Tool = SAST
3. Click on 'Description' any Vulnerability'
4. Click on 'Explain vulnerability' to understand the vulnerability, or click on 'Resolve with AI' to create a merge request that resolves the vulnerability.

## Use `Value stream forecasting`
1. On the menu bar, click on 'Analyze' >> 'CI/CD Analytics' >> 'Deployment frequency'
2. Turn on 'Show forecast' to predict frequency of deployments to production environment

## Use `Gitlab Duo Chat`
1. You can ask about a specific questions, GitLab Duo Chat can help in a variety of areas

     ``` yaml
     - How do I connect to a Kubernetes cluster to Gitlab

    - Provide step-by-step instructions on how to reset a user's password

    - Explain this error message in Java : Int and system cannot be resolved to a type
      ```  